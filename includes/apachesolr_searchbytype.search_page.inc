<?php

/**
 * @file
 * Include file for Apache Solr Search by Type page searches.
 */

/**
 * Drupal form for Apache Solr Search by Type pages.
 */
function apachesolr_searchbytype_search_page($node_type, $path) {
  $output = '';
  $keys = trim(apachesolr_searchbytype_get_keys($path));

  // Do search
  $filters = isset($_GET['filters']) ? $_GET['filters'] : 'type:' . check_plain($node_type->type);
  $solrsort = isset($_GET['solrsort']) ? $_GET['solrsort'] : '';
  $page = isset($_GET['page']) ? $_GET['page'] : 0;
  try {
    module_load_include('inc', 'search', 'search.pages');
    $results = apachesolr_search_execute($keys, $filters, $solrsort, $path, $page);
  }
  catch (Exception $e) {
    watchdog('Apache Solr', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
    apachesolr_failure(t('Solr search'), $keys);
  }
  
  // Construct the search form and results
  $output .= drupal_get_form('apachesolr_searchbytype_search_form', $node_type, $path);
  if (count($results) > 0) {
    $output .= theme('search_results', $results, 'node');
  }
  
  return $output;
}