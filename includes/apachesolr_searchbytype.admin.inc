<?php

/**
 * @file
 * Include file for Apache Solr Search by Type admin settings.
 */

/**
 * Drupal admin form for Apache Solr Search by Type settings.
 */
function apachesolr_searchbytype_admin_settings($form_state) {
  $form = array();
  
  // Search pages paths
  $form['apachesolr_searchbytype_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page paths'),
    '#description' => t('Set the page paths for each search by type.  Leave blank to disable the search.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $types = node_get_types();
  foreach ($types as $type => $typeobj) {
    $form['apachesolr_searchbytype_pages']['apachesolr_searchbytype_' . $type] = array(
      '#type' => 'textfield',
      '#title' => t('Path for %name', array('%name' => $typeobj->name)),
      '#default_value' => variable_get('apachesolr_searchbytype_' . $type, ''),
    );
  }
  
  // Search Pages titles
  $form['apachesolr_searchbytype_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search titles'),
    '#description' => t('Set the titles for searches.  These will be used for block and page titles.  The default is "Search @Type".'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  foreach ($types as $type => $typeobj) {
    $form['apachesolr_searchbytype_titles']['apachesolr_searchbytype_title_' . $type] = array(
      '#type' => 'textfield',
      '#title' => t('Title for %name', array('%name' => $typeobj->name)),
      '#default_value' => variable_get('apachesolr_searchbytype_title_' . $type, t('Search @type', array('@type' => $typeobj->name))),
    );
  }
  
  // Search for type block
  $form['apachesolr_searchbytype_searchblock_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Block'),
    '#description' => t('Once you enable at least one content type below, there will be a block available where a user can choose to search the whole site or a specific type of content.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $type_array = array();
  foreach ($types as $type => $typeobj) {
    $type_array[$type] = $typeobj->name;
  }
  $form['apachesolr_searchbytype_searchblock_fieldset']['apachesolr_searchbytype_searchblock'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable types'),
    '#options' => $type_array,
    '#default_value' => variable_get('apachesolr_searchbytype_searchblock', array()),
  );

  // Search block titles
  $form['apachesolr_searchbytype_block_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Block titles'),
    '#description' => t('Set the titles for the content types that are part of the search block.  The default is name of the content type.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  foreach ($types as $type => $typeobj) {
    $form['apachesolr_searchbytype_block_titles']['apachesolr_searchbytype_block_titles_' . $type] = array(
      '#type' => 'textfield',
      '#title' => t('Title for %name', array('%name' => $typeobj->name)),
      '#default_value' => variable_get('apachesolr_searchbytype_block_titles_' . $type, check_plain($typeobj->name)),
    );
  }
  
  $form['#submit'][] = 'apachesolr_searchbytype_admin_settings_submit';
  $form = system_settings_form($form);
  return $form;
}

/**
 * Validate admin form.
 */
function apachesolr_searchbytype_admin_settings_validate($form, &$form_state) {
  // Make sure that there are paths for enabled multi-search block.
  $enabled = $form_state['values']['apachesolr_searchbytype_searchblock'];
  foreach (array_filter($enabled) as $enabled_type) {
    if (empty($form_state['values']['apachesolr_searchbytype_' . $enabled_type])) {
      form_set_error('apachesolr_searchbytype_' . $enabled_type, t('If the content type is enabled for the main search block, ensure there is a path set for that search type.'));
    }
  }
}

/**
 * Submit function for admin settings so that we can rebuild the menu.
 */
function apachesolr_searchbytype_admin_settings_submit($form, &$form_state) {
  menu_rebuild();
}