
## About ##

This is a Drupal module that creates pages and blocks for
searching via Apache Solr specific to each content type that
is defined on a site.

## Features ##

This module has the following feature

  * Define new pages to run searches for each content type.
  * Provides blocks for each type that has a page defined.
  * Provides a multi-search block where users can search the whole site or specific content types.
  
## Dependencies ##

Apache Solr (http://drupal.org/project/apachesolr) 6.x-2.x
(This module could work with Apache Solr 1.x, but this has not been tested)

## Author(s) ##

  * Alan Palazzolo (zzolo) http://drupal.org/user/147331