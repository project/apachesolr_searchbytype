<?php

/**
 * @file
 * Main module file for Apache Solr Search by Type module.
 *
 * This module provides blocks and pages for custom
 * Apache Solr searches by content types.
 */
 
/**
 * Block prefix
 */
define('APACHESOLR_SEARCHBYTYPE_BLOCK_PREFIX', 'ap_sbt_');

/**
 * Implements hook_help().
 */
function apachesolr_searchbytype_help($path, $arg) {
  $output = '';
  
  switch ($path) {
    case 'admin/help#apachesolr_searchbytype':
      $output = '<p>' . t('This module provides blocks and pages for custom Apache Solr searches by content types.') . '</p>';
      break;
      
    case 'admin/settings/apachesolr/searchbytype':
      $output = '<p>' . t('Configurations for pages and blocks to search by content type.') . '</p>';
      break;

  }
  
  return $output;
}

/**
 * Implements hook_menu().
 */
function apachesolr_searchbytype_menu() {
  $items = array();
  
  $items['admin/settings/apachesolr/searchbytype'] = array(
    'title' => 'Search by Type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('apachesolr_searchbytype_admin_settings'),
    'access arguments' => array('administer search'),
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/apachesolr_searchbytype.admin.inc',
  );
  
  $types = node_get_types();
  foreach ($types as $type => $typeobj) {
    $path = variable_get('apachesolr_searchbytype_' . $type, '');
    if (!empty($path)) {
      $items[$path] = array(
        'title' => check_plain(variable_get('apachesolr_searchbytype_title_' . $type, t('Search @type', array('@type' => $typeobj->name)))),
        'page callback' => 'apachesolr_searchbytype_search_page',
        'page arguments' => array($typeobj, $path),
        'access arguments' => array('search content'),
        'file' => 'includes/apachesolr_searchbytype.search_page.inc',
      );
    }
  }
  
  return $items;
}

/**
 * Implements hook_block().
 */
function apachesolr_searchbytype_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      // Make blocks for content types that have paths set.
      $blocks = array();
      $types = node_get_types();
      foreach ($types as $type => $typeobj) {
        $path = variable_get('apachesolr_searchbytype_' . $type, '');
        if (!empty($path)) {
          $blocks[APACHESOLR_SEARCHBYTYPE_BLOCK_PREFIX . check_plain($type)] = array(
            'info' => t('Apache Solr Search by Type: @type', array('@type' => $typeobj->name)),
          );
        }
      }
      
      // Block for main search
      $enabled = variable_get('apachesolr_searchbytype_searchblock', array());
      if (!empty($enabled)) {
        $blocks['ap_sbt_main_search'] = array(
          'info' => t('Apache Solr Search by Type: Multi-Search'),
        );
      }
      
      return $blocks;
      
    case 'view':
      $block = array();
      if ($delta == 'ap_sbt_main_search') {
        $enabled = variable_get('apachesolr_searchbytype_searchblock', array());
        if (!empty($enabled)) {
          $block['subject'] = t('Search site');
          $block['content'] = drupal_get_form('apachesolr_searchbytype_multisearch_form');
        }
      }
      else {      
        $type = str_replace(APACHESOLR_SEARCHBYTYPE_BLOCK_PREFIX, '', $delta);
        $path = variable_get('apachesolr_searchbytype_' . $type, '');
        if (!empty($type) && !empty($path)) {
          $typeobj = node_get_types('type', $type);
          $block['subject'] = check_plain(variable_get('apachesolr_searchbytype_title_' . $type, t('Search @type', array('@type' => $typeobj->name))));
          $block['content'] = drupal_get_form('apachesolr_searchbytype_search_form', $type, $path);;
        }
      }
      return $block;
      
  }
}

/**
 * General search by type form
 */
function apachesolr_searchbytype_search_form(&$form_state, $type, $redirect) {
  $form = array();
  $keys = apachesolr_searchbytype_get_keys($redirect);

  $form['keywords'] = array(
    '#title' => t('Enter keywords'),
    '#type' => 'textfield',
    '#default_value' => !empty($keys) ? $keys : '',
  );
  $form['search_redirect'] = array(
    '#type' => 'hidden',
    '#value' => $redirect,
  );
  $form['nodetype'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  
  $form['#submit'][] = 'apachesolr_searchbytype_search_form_submit';
  return $form;
}

/**
 *
 */
function apachesolr_searchbytype_search_form_submit($form, &$form_state) {
  // The search form relies on control of the redirect destination for its
  // functionality, so we override any static destination set in the request,
  // for example by drupal_access_denied() or drupal_not_found()
  // (see http://drupal.org/node/292565).
  if (isset($_REQUEST['destination'])) {
    unset($_REQUEST['destination']);
  }
  if (isset($_REQUEST['edit']['destination'])) {
    unset($_REQUEST['edit']['destination']);
  }

  $keys = $form_state['values']['keywords'];
  // Handle Apache webserver clean URL quirks.
  if (variable_get('clean_url', '0')) {
    $keys = str_replace('+', '%2B', $keys);
  }
  
  // Check node type
  if (is_object($form_state['values']['nodetype'])) {
    $type = $form_state['values']['nodetype']->type;
  }
  else {
    $type = (string) $form_state['values']['nodetype'];
  }
  
  // Redirect to configured path with type filtered.
  $form_state['redirect'] = array(
    $form_state['values']['search_redirect'] . '/' . trim($keys),
    'filters=type:' . $type,
  );
}

/**
 * 
 */
function apachesolr_searchbytype_multisearch_form($form_state) {
  $form = array();
  $enabled = variable_get('apachesolr_searchbytype_searchblock', array());
  $types = array('all' => t('Site'));
  foreach (array_filter($enabled) as $type) {
    $typeobj = node_get_types('type', $type);
    $types[$type] = variable_get('apachesolr_searchbytype_block_titles_' . $type, check_plain($typeobj->name));
  }
  
  $form['keywords'] = array(
    '#title' => t('Enter keywords'),
    '#type' => 'textfield',
    '#default_value' => '',
  );
  $form['search_type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#options' => $types,
    '#default_value' => isset($form_state['post']['search_type']) ? $form_state['post']['search_type'] : 'all',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  
  return $form;
}

/**
 *
 */
function apachesolr_searchbytype_multisearch_form_submit($form, &$form_state) {
  // The search form relies on control of the redirect destination for its
  // functionality, so we override any static destination set in the request,
  // for example by drupal_access_denied() or drupal_not_found()
  // (see http://drupal.org/node/292565).
  if (isset($_REQUEST['destination'])) {
    unset($_REQUEST['destination']);
  }
  if (isset($_REQUEST['edit']['destination'])) {
    unset($_REQUEST['edit']['destination']);
  }

  $keys = $form_state['values']['keywords'];
  // Handle Apache webserver clean URL quirks.
  if (variable_get('clean_url', '0')) {
    $keys = str_replace('+', '%2B', $keys);
  }
  
  // Determine path and filter
  if ($form_state['values']['search_type'] == 'all') {
    $path = 'search/apachesolr_search';
    $type = '';
  }
  else {
    $path = variable_get('apachesolr_searchbytype_' . $form_state['values']['search_type'], '');
    $type = 'filters=type:' . $form_state['values']['search_type'];
  }
  
  // Redirect to configured path with type filtered.
  $form_state['redirect'] = array($path . '/' . trim($keys), $type);
}

/**
 * Function to get the search keywords from a path.
 */
function apachesolr_searchbytype_get_keys($path) {
  $found = str_replace($path . '/', '', $_GET['q']);
  return ($_GET['q'] != $found) ? $found : '';
}